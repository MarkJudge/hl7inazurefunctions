using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using HL7Enumerator;
using System.Diagnostics;
namespace SystemC.Learning
{
    public static class HL7Enumerator
    {
        [FunctionName("HL7Enumerator")] 
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "v1/HL7Enumerator")] HttpRequest req,
            ILogger log)
        {
            var watch = Stopwatch.StartNew();
            log.LogInformation("Request recieved");
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            log.LogTrace(requestBody);
            HL7Message message = null;
            bool isParsed = false;
            IActionResult result = null;
            try
            {
                message = new HL7Message(requestBody);
                isParsed = true;
            }
            catch (Exception ex)
            {
                log.LogError("Unable To Parse Message: {ParseError}",ex.Message);
                result = new BadRequestObjectResult($"Unable To Parse Message: {ex.Message}");
            }
            if (isParsed)
            {
                var type = message.Element("MSH.9");
                var structure = message.Element("MSH.9.3");
                log.LogInformation("Message Type: {MessageType}, Structure:", structure);

                var resultText = new StringWriter();
                resultText.WriteLine("Doesn't Seem to support writing messages");

                watch.Stop();
                resultText.WriteLine($"Executed in {watch.ElapsedMilliseconds} ms");
                result = new OkObjectResult(resultText.ToString());
            }
            
            return result;
        }


    }
}
