using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using HL7.Dotnetcore;
using System.Diagnostics;
namespace Systemc.AzureFunctionLearning
{
    public static class HttpTrigger1
    {
        [FunctionName("HL7_Dotnetcore")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "v1/HL7.Dotnetcore")] HttpRequest req,
            ILogger log)
        {
            var watch = Stopwatch.StartNew();
            log.LogInformation("Request recieved");
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            log.LogTrace(requestBody);

            Message message = new Message(requestBody);
            bool isParsed = false;
            IActionResult result = null;
            try{
                isParsed = message.ParseMessage();
            }catch(Exception ex)
            {
                log.LogError("Unable To Parse Message: {ParseError}",ex.Message);
                result = new BadRequestObjectResult($"Unable To Parse Message: {ex.Message}");
            }

            if(isParsed){
                log.LogInformation("Message Type: {MessageType}", message.GetValue("MSH.9"));
                var ack = message.GetACK();
                var nack = message.GetNACK("100", "A NACK for testing purposes");
                var resultText = new StringWriter();
                resultText.WriteLine(ack.SerializeMessage(true)) ;
                resultText.WriteLine();
                resultText.WriteLine(nack.SerializeMessage(true));
                resultText.WriteLine($"Executed in {watch.ElapsedMilliseconds} ms");
                result = new OkObjectResult(resultText.ToString());
                watch.Stop();
                
            }
            return result;
        }
    }
}
