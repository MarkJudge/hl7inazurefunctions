using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Mergecom.HL7;
using System.Diagnostics;
using Mergecom.HL7.V231.Segments;
using Mergecom.HL7.V231.Fields;

namespace Systemc.AzureFunctionLearning
{
    public static class MergeCom_HL7
    {
        [FunctionName("MergeCom_HL7")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "v1/MergeCom_HL7")] HttpRequest req,
            ILogger log)
        {
            var watch = Stopwatch.StartNew();
            HL7Toolkit.Properties["LICENSE_KEY"]="wiqd8RLo/QAAax4AAFAUGb4EuNTXEDXMRhb1f6FOqp5lOzIU6qPo+2MldvERxpLuzlgysfnazJrQBHikunwBirAChC6/nN73/Cr1rRznN5rcMPwkklo9cX2CizIO7Sk46u7qTeCvVVoTJBfcLkzOUswwu+QFtVJyKwbJ2QgrIuazRz3krBn+ud6BZHTvQ5C7W3k=";
            log.LogInformation("Request recieved");
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            log.LogTrace(requestBody);

            HL7Message message = null;
            bool isParsed = false;
            IActionResult result = null;
            try {
                message = new HL7Message(requestBody);
                isParsed = true;
            } catch (Exception ex)
            {
                log.LogError("Unable To Parse Message: {ParseError}", ex.Message);
                result = new BadRequestObjectResult($"Unable To Parse Message: {ex.Message}");
            }

            if (isParsed) {
                log.LogInformation("Message Type: {MessageType}", message.GetFieldValue("MSH.9"));

                var ack = CreateAck(message,"AA");
                var nack = CreateAck(message,"AE",  "A NACK for testing purposes");
                var resultText = new StringWriter();
                resultText.WriteLine(ack.GetContent());
                resultText.WriteLine();
                resultText.WriteLine(nack.GetContent());
                resultText.WriteLine($"Executed in {watch.ElapsedMilliseconds} ms");
                result = new OkObjectResult(resultText.ToString());
                watch.Stop();

            }
            return result;
        }
        private static HL7Message CreateAck(HL7Message msg,string code,string errMsg = null) {
            var ack = new HL7Message();
            ack.AddSegment(CreateMSH());

            ack.AddSegment(CreateMSA(code));
            if (errMsg != null)
            {
                ack.AddSegment(CreateErr(errMsg));
            }
            ack.SetFieldValue("MSH.9.3", ack.GetFieldValue("MSH.9.3"));
            return ack;
        }

        private static HL7Segment CreateMSH()
        {
            var msh = new HL7SegmentMSH();
            HL7FieldHD sendingApp = (HL7FieldHD)msh.GetSendingApplication();
            sendingApp.NamespaceID = "Test";
           
            return msh;
        }
        private static HL7Segment CreateMSA(string code)
        {
            var msa = HL7Segment.CreateSegment("MSA");
            msa.SetFieldValue(1, code);
            msa.SetFieldValue(2, "1817457");
            return msa;
        }
        private static HL7Segment CreateErr(string errMsg)
        {
            var msa = HL7Segment.CreateSegment("ERR");
            msa.SetFieldValue(1, errMsg);
            return msa;
        }

    }
}
