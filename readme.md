# Hl7 Nuget Package Investigation
A simple Azure function app to investigate the uses of hl7 parser packages

The following packages were investigated and POC's built in most cases.

All Scores are from 0-5 with 5 being the most desirable (ie 5 for cost = free)


## **nHapi** 

[Github](https://github.com/nHapiNET/nHapi/tree/master/src) | [Proof Of Concept](nHapi.cs)
### **Pros**
 - Strongly Typed
 - Large user and developer base
### **Cons**
 - Complicated
 - Slower
 - Poor documentation for nHapi (but it's based on Hapi (java library which has ok documentation))

### **Scores**
| Area                                  | Score  |
| --------------------------------------|:------:|
| **Support/Development Confidence**    |   4    |
| **Documentation**                     |   3    |
| **Cost**                              |   5    |
| **Ease of Use**                       |   3    |
| **Feature Set**                       |   4    |
| **Total**                             | **19** |

____________________________________________________________________________
## **HL7.dotnetcore**

[Github](https://github.com/Efferent-Health/HL7-dotnetcore) | [Proof Of Concept](HL7.Dotnetcore.cs)
### **Pros**
 - Fast and Light
 - Simple Code, could easily be forked to meet our needs/address issues further down the line
 - .NET Core
### **Cons**
 - Weakly Typed (All values are strings)



### **Scores**
| Area                                  | Score  |
| --------------------------------------|:------:|
| **Support/Development Confidence**    |   1    |
| **Documentation**                     |   3    |
| **Cost**                              |   5    |
| **Ease of Use**                       |   3    |
| **Feature Set**                       |   1    |
| **Total**                             | **13** |


____________________________________________________________________________
## **Galkam.HL7Enumerator**

[Galkam.HL7Enumerator](https://github.com/glenkleidon/HL7Enumerator/) | [Proof Of Concept](HL7Enumerator.cs)
### **Pros**
 - Fast and Light
### **Cons**
 - Weakly Typed (All values are strings)
 - Modification of messages is possible but very limited.

### **Scores**
| Area                                  | Score  |
| --------------------------------------|:------:|
| **Support/Development Confidence**    |   1    |
| **Documentation**                     |   3    |
| **Cost**                              |   5    |
| **Ease of Use**                       |   3    |
| **Feature Set**                       |   1    |
| **Total**                             | **13** |
____________________________________________________________________________

## **Merge HL7 Toolkit**

[Merge HL7 Toolkit](https://www.ibm.com/products/merge-hl7-toolkit/) | [Proof Of Concept](MergeCom_HL7.cs)
### **Pros**
 - Supported & Maintained By IBM
 - Allows Complex operations like mappings using ini files
 - Type Message, with code generation for custom message profiles
 - Fairly well documented
### **Cons**
 - Cost Unknown
 - Possibly Overkill, includes ability, to send/recieve messages

### **Scores**
| Area                                  | Score  |
| --------------------------------------|:------:|
| **Support/Development Confidence**    |   4    |
| **Documentation**                     |   3    |
| **Cost**                              |   1    |
| **Ease of Use**                       |   3    |
| **Feature Set**                       |   5    |
| **Total**                             | **16** |


## **Lyniate SDK**

Currently unable to create Proof-of-concept, not sure it's possible & don't have a licence.

### **Pros**
 - Supported & Maintained By Lyniate
 - Known, and used withing Medway
 - We already have our on profiles which are configurable using a GUI
 - Good Documentation
### **Cons**
 - Cost Unknown
 - May not be possible

### **Scores**
| Area                                  | Score  |
| --------------------------------------|:------:|
| **Support/Development Confidence**    |   3    |
| **Documentation**                     |   5    |
| **Cost**                              |   1    |
| **Ease of Use**                       |   5    |
| **Feature Set**                       |   5    |
| **Total**                             | **16** |

____________________________________________________________________________
 ## ~~**Machete.HL7**~~

[Github](https://www.nuget.org/packages/Machete.HL7/1.0.245-develop)
### **Pros**
 - Looks like a nice package for parsing HL7 messages
### **Cons**
 - Does not allow modification/creation - did not build investigate further or build a POC due this.
