using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NHapi.Base.Parser;
using NHapi.Base.Model;
using NHapi.Model.V231.Message;
using NHapiTools.Base.IO;
using NHapiTools.Base.Util; 
using System.Diagnostics;
namespace SystemC.Learning
{
    public static class nHapi
    {
        [FunctionName("nHapi")] 
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "v1/nHapi")] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("Request recieved");
            var watch = Stopwatch.StartNew();
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            log.LogTrace(requestBody);
            var parser = new PipeParser();
            IMessage message = null;
            bool isParsed = false;
            IActionResult result = null;
            try
            {
                message = parser.Parse(requestBody);
                isParsed = true;
            }
            catch (Exception ex)
            {
                log.LogError("Unable To Parse Message: {ParseError}",ex.Message);
                result = new BadRequestObjectResult($"Unable To Parse Message: {ex.Message}");
            }
            if (isParsed)
            {
                var msh = message.GetStructure("MSH") as ISegment;
                var type = msh.GetField(9);
                log.LogInformation("Message Type: {MessageType}, Structure:", type,message.GetStructureName());
                var ack = new Ack("TestApplication", "Development").MakeACK(message);
                var nack= new Ack("TestApplication", "Development").MakeACK(message,AckTypes.AE, "A NACK for testing purposes");

                var resultText = new StringWriter();
                resultText.WriteLine(parser.Encode(ack));
                resultText.WriteLine();
                resultText.WriteLine(parser.Encode(nack));
                resultText.WriteLine($"Executed in {watch.ElapsedMilliseconds} ms");
                result = new OkObjectResult(resultText.ToString());
                watch.Stop();
            }
            return result;
        }
    }
}
